"""
File:         ui/motors_tab.py
Author:       Eliott Sarrey
Date:         June 2023
Email:        eliott.sarrey@gmail.com

Description:
    This file imports the necessary modules and defines various classes related to motor control and its interface.
    It allows to easily move each motors using arrows (with 3 different possible speed),
    to home all or a specific axis and to go directly to a position specified by the user.
"""

from PyQt5 import QtWidgets
from PyQt5.QtGui import QFont
from pymeasure.display.widgets import TabWidget

from src.classes import stage
from src.classes import stage_config

class ArrowButton(QtWidgets.QPushButton):
    def __init__(self, direction, step, size=20, box_size=(26, 40), parent=None):
        """
        Initialize an ArrowButton object.

        Args:
            direction (str):    The direction of the arrow button ('left', 'right', '0').
            step (float):       The step size associated with the button.
            size (int):         The font size of the button.
            box_size (tuple):   The size of the button's bounding box (width, height).
            parent (QWidget):   The parent widget. (default: None)
        """
        super().__init__(parent)
        self.direction = direction
        self.step = step
        self.setFixedSize(*box_size)

        if self.direction == "0":
            self.setText("Home")
        else:
            font = QFont("Calibri")
            self.setStyleSheet(f"font: {size}pt;")
            self.setFont(font)

            if self.direction == "left":
                self.setText("←")
            elif self.direction == "right":
                self.setText("→")

class ArrowButtonGroup(QtWidgets.QWidget):
    def __init__(self, axis, parent=None):
        """
        Initialize an ArrowButtonGroup object.

        Args:
            axis (str): The axis associated with the group ('x', 'y', 'z')
            parent (QWidget): The parent widget. (default: None)
        """
        super().__init__()
        self.axis = axis
        self.initUI()

    def initUI(self):
        """
        Create all the buttons in the box: 7 in total with 6 arrows for 3 speeds in the 2 directions
        and a homing button.
        """
        box = QtWidgets.QHBoxLayout()

        self.setFixedSize(400, 80)

        step_sizes = stage_config.get_section("Control")

        button1 = ArrowButton("left", -float(step_sizes.get("step_mm_L", 0)), 20, (40, 40))
        button2 = ArrowButton("left", -float(step_sizes.get("step_mm_M", 0)), 14, (32, 40))
        button3 = ArrowButton("left", -float(step_sizes.get("step_mm_S", 0)), 10, (22, 40))
        button4 = ArrowButton("0", 0, 15, (80, 40))
        button5 = ArrowButton("right", float(step_sizes.get("step_mm_S", 0)), 10, (22, 40))
        button6 = ArrowButton("right", float(step_sizes.get("step_mm_M", 0)), 14, (32, 40))
        button7 = ArrowButton("right", float(step_sizes.get("step_mm_L", 0)), 20, (40, 40))

        button1.clicked.connect(self.on_button_click)
        button2.clicked.connect(self.on_button_click)
        button3.clicked.connect(self.on_button_click)
        button4.clicked.connect(self.on_home_click)
        button5.clicked.connect(self.on_button_click)
        button6.clicked.connect(self.on_button_click)
        button7.clicked.connect(self.on_button_click)

        box.addWidget(button1)
        box.addWidget(button2)
        box.addWidget(button3)
        box.addWidget(button4)
        box.addWidget(button5)
        box.addWidget(button6)
        box.addWidget(button7)

        self.setLayout(box)

    def on_button_click(self):
        """
        Handle the button click event and perform the corresponding motor movement.
        """
        button = self.sender()
        if self.axis == 'x':
            self.parent().move_x(button.step)
        elif self.axis == 'y':
            self.parent().move_y(button.step)
        elif self.axis == 'z':
            self.parent().move_z(button.step)

    def on_home_click(self):
        """
        Handle the home button click event and perform the corresponding axis homing.
        """
        self.parent().home_axis(self.axis)

class MotorsTab(TabWidget, QtWidgets.QWidget):
    def __init__(self, name, parent=None):
        """
        Initialize a MotorsTab object.

        Args:
            name (str): The name of the tab.
            parent (QWidget): The parent widget.
        """
        super().__init__(parent)
        self.name = name

        font = QFont()
        font.setPointSize(16)

        layout = QtWidgets.QGridLayout()

        layout.addWidget(QtWidgets.QLabel("Current Position", self), 0, 0, 1, 6)

        self.x_value = QtWidgets.QLineEdit(stage.get_x_pos_str())
        self.x_value.setEnabled(False)
        self.x_value.setStyleSheet("color: white")
        layout.addWidget(self.x_value, 1, 0, 1, 2)

        self.y_value = QtWidgets.QLineEdit(stage.get_y_pos_str())
        self.y_value.setEnabled(False)
        self.y_value.setStyleSheet("color: white")
        layout.addWidget(self.y_value, 1, 2, 1, 2)

        self.z_value = QtWidgets.QLineEdit(stage.get_z_pos_str())
        self.z_value.setEnabled(False)
        self.z_value.setStyleSheet("color: white")
        layout.addWidget(self.z_value, 1, 4, 1, 2)

        self.homing = QtWidgets.QPushButton("Home Axis")
        layout.addWidget(self.homing, 2, 0, 1, 6)

        layout.addWidget(QtWidgets.QLabel("Target Position", self), 3, 0, 1, 6)

        self.x_input = QtWidgets.QLineEdit()
        self.x_input.setText("0")
        layout.addWidget(self.x_input, 4, 0, 1, 2)

        self.y_input = QtWidgets.QLineEdit()
        self.y_input.setText("0")
        layout.addWidget(self.y_input, 4, 2, 1, 2)

        self.z_input = QtWidgets.QLineEdit()
        self.z_input.setText("0")
        layout.addWidget(self.z_input, 4, 4, 1, 2)

        self.go_button = QtWidgets.QPushButton("Go")
        layout.addWidget(self.go_button, 5, 0, 1, 6)

        label_arrows_X = QtWidgets.QLabel("X Position", self)
        label_arrows_Y = QtWidgets.QLabel("Y Position", self)
        label_arrows_Z = QtWidgets.QLabel("Z Position", self)

        layout.addWidget(label_arrows_X, 6, 0, 1, 3)
        layout.addWidget(label_arrows_Y, 7, 0, 1, 3)
        layout.addWidget(label_arrows_Z, 8, 0, 1, 3)

        group1 = ArrowButtonGroup('x')
        group2 = ArrowButtonGroup('y')
        group3 = ArrowButtonGroup('z')

        layout.addWidget(group1, 6, 3, 1, 3)
        layout.addWidget(group2, 7, 3, 1, 3)
        layout.addWidget(group3, 8, 3, 1, 3)

        self.setLayout(layout)

        self.homing.clicked.connect(self.home_all)
        self.go_button.clicked.connect(self.go_to_position)

    def go_to_position(self):
        """
        Move the stage to the target position specified by the user.
        """
        x = float(self.x_input.text())
        y = float(self.y_input.text())
        z = float(self.z_input.text())

        stage.move_x_to(x)
        stage.move_y_to(y)
        stage.move_z_to(z)

        self.update_positions()

    def home_all(self):
        """
        Home all axes of the stage.
        """
        stage.home_axis()
        self.update_positions()

    def home_axis(self, axis):
        """
        Home a specific axis of the stage.

        Args:
            axis (str): The axis to home ('x', 'y', 'z').
        """
        stage.home_axis(axis)
        self.update_positions()

    def update_positions(self):
        """
        Update the displayed positions of the stage axes.
        """
        self.x_value.setText(str(stage.get_x_pos_str()))
        self.y_value.setText(str(stage.get_y_pos_str()))
        self.z_value.setText(str(stage.get_z_pos_str()))

    def move_x(self, distance):
        """
        Move the stage in the X-axis by the specified distance.

        Args:
            distance (float): The distance to move in millimeters.
        """
        stage.move_x(distance)
        self.update_positions()

    def move_y(self, distance):
        """
        Move the stage in the Y-axis by the specified distance.

        Args:
            distance (float): The distance to move in millimeters.
        """
        stage.move_y(distance)
        self.update_positions()

    def move_z(self, distance):
        """
        Move the stage in the Z-axis by the specified distance.

        Args:
            distance (float): The distance to move in millimeters.
        """
        stage.move_z(distance)
        self.update_positions()