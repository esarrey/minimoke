"""
File:         ui/__init__.py
Author:       Eliott Sarrey
Date:         June 2023
Email:        eliott.sarrey@gmail.com

Description:
    This file imports all the classes required for the UI
"""

from .motors_tab_ui      import MotorsTab
from .live_tab_ui        import LiveTab
from .main_ui            import UIWindow
from .usermanual_tab_ui  import UserManualTab