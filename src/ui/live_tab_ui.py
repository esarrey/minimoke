from time import sleep
import numpy as np
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QGridLayout, QLabel, QWidget, QPushButton, QApplication
from pymeasure.display.widgets import TabWidget
from PyQt5.QtCore import QThread, pyqtSignal, Qt

from src.classes import dac
from src.classes import log
from src.classes import hall_sensor

class HallSensorThread(QThread):
    sensor_data_signal = pyqtSignal(float)

    def __init__(self):
        """
        Constructor method for the hall sensor thread.
        """
        super().__init__()

    def run(self):
        """
        The function continuously reads sensor data from a hall sensor and emits a signal with the data
        if the sensor is not reserved to be displayed in this tab.
        """
        while True:
            if not hall_sensor.reserved:
                sensor_data = hall_sensor.read_mT()
                if sensor_data: self.sensor_data_signal.emit(sensor_data)
            else:
                sleep(1)

    def zero_sensor_value(self):
        """
        Zero the Hall sensor and logs the completion.
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        hall_sensor.zeroing()
        QApplication.restoreOverrideCursor()
        log.info("Zeroing Hall sensor done")

class VoltageThread(QThread):
    balanced_diodes_signal = pyqtSignal(dict)

    def __init__(self):
        """
        Constructor method for a the voltage thread.
        """
        super().__init__()

    def run(self):
        """
        The function continuously checks if the DAC is not reserved and if the status setup is not complete,
        then it sets up the acquisition and starts the tasks, reads the data from the DAC, calculates the
        mean of the balanced diodes data and intensity diode data, and emits a signal with the output 
        to be lived display in this tab.
        """
        while True:
            if not dac.reserved:
                if not dac.status_setup:
                    dac.setup_aquisition(acquisition_time=0.5, modulation_amp=[0.,0.])
                dac.start_tasks()
                balanced_diodes_data, intensity_diode_data = dac.read_data()
                output = {"bd": np.mean(balanced_diodes_data), "id": np.mean(intensity_diode_data)}
                self.balanced_diodes_signal.emit(output)
            else:
                sleep(1)

class LiveTab(TabWidget, QWidget):
    def __init__(self, name, parent=None):
        """
        The function initializes a GUI tab with labels and values for balanced diodes voltage, intensity
        diode voltage, and magnetic field.

        Args:
            name (str): the name of the tab
        """
        super().__init__(parent)
        self.name = name

        # Create label widgets
        self.bd_value_label = QLabel('Balanced diodes voltage:', self)
        self.bd_value_label.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.bd_value = QLabel('Waiting for diodes data...', self)
   
        self.id_value_label = QLabel('Intensity diode voltage:', self)
        self.id_value_label.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.id_value = QLabel('Waiting for diode data...', self)

        self.hall_sensor_label = QLabel('Magnetic field:', self)
        self.hall_sensor_label.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.hall_sensor_value = QLabel('Waiting for the sensor data...', self)

        font = QFont()
        font.setPointSize(48)
        layout = QGridLayout()

        # Add the label widgets to the layout
        layout.addWidget(self.id_value_label, 0, 0)
        layout.addWidget(self.id_value, 0, 1)
        self.id_value.setFont(font)

        layout.addWidget(self.bd_value_label, 1, 0)
        layout.addWidget(self.bd_value, 1, 1)
        self.bd_value.setFont(font)

        layout.addWidget(self.hall_sensor_label, 2, 0)
        layout.addWidget(self.hall_sensor_value, 2, 1)
        self.hall_sensor_value.setFont(font)

        # Create the threads for the asynchronous update of the labels
        self.voltage_thread = VoltageThread()
        self.voltage_thread.balanced_diodes_signal.connect(self.update_voltage_value)
        self.voltage_thread.start()

        self.hall_sensor_thread = HallSensorThread()
        self.hall_sensor_thread.sensor_data_signal.connect(self.update_sensor_value)
        self.hall_sensor_thread.start()

        # Create the button for the hall sensor zeroing
        self.zero_button = QPushButton("Hall sensor zeroing")
        layout.addWidget(self.zero_button, 3, 0, 1, 2)
        self.zero_button.clicked.connect(self.hall_sensor_thread.zero_sensor_value)

        self.setLayout(layout)


    def update_sensor_value(self, sensor_value):
        """
        The function updates the value of a hall sensor and displays it in a text field.
        
        Args:
            sensor_value (float): the value of the sensor reading in millitesla (mT)
        """
        self.hall_sensor_value.setText(f'{sensor_value:.6f}mT')

    def update_voltage_value(self, voltage_value): 
        """
        The function `update_voltage_value` updates the text values of `bd_value` and `id_value` with the
        corresponding voltage values in millivolts.
        
        Args:
            voltage_value (dict) : a dictionary that contains two keys: "bd" and "id". The values
                                    associated with these keys represent the voltage values of the
                                    balanced and intensity diodes respectively.
        """
        self.bd_value.setText(f'{(voltage_value["bd"]*1000.):.1f} mV')
        self.id_value.setText(f'{(voltage_value["id"]*1000.):.1f} mV')