"""
File:         procedures/x_sweep_proc.py
Author:       Eliott Sarrey
Date:         June 2023
Email:        eliott.sarrey@gmail.com

Description: 
    Define the full experiment procedure for a sweep along the X axis.
    This file defines the necessary parameters, which are automatically used by
    the UI thanks to pymeasure.
    Then, it proceed to the main loop and perform the DC and AC measurements of the voltage
    and save many other values. It also modulates the magnetic field at a given frequency.
    All the results are saved in the DATA_COLUMNS object and plotted live in the main UI.
"""

import time
import numpy as np

from pymeasure.experiment import Procedure, FloatParameter, Metadata, ListParameter

from src.classes import stage, dac, hall_sensor, log
from src.classes import proc_config, dac_config


class Y_Sweep(Procedure):
    """
    Procedure for a sweep of the magnetic field
    """
    name = "Y-Sweep"                                    # Define the name of the procedure

    # Create metadata objects, values will be stored during the startup
    exp_type_md     = Metadata("Experiment type")
    sample_md       = Metadata("Sample name")
    nb_it_md        = Metadata("Total number of iterations (if success)")
    time_md   = Metadata("Beginning of experiment", fget=lambda: time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())))

    # Define the AC channels
    AC_ports    = dac_config.get_section('IO ports')
    AC_chan    = [f'AC_Output1 ({AC_ports.get("AC_Output1", "None")})', f'AC_Output2 ({AC_ports.get("AC_Output2", "None")})', 'None']

    section = proc_config.get_section(name)             # Get the saved parameters values from the config file

    # Define all the parameters with their type (float, bool), name, units and minimum and maximum
    acq_time    = FloatParameter('Aquisition time',         units='s',  default=section.get("acq_time", 1),     minimum=1e-6)
    freq        = FloatParameter('Field modulation Freq',   units='Hz', default=section.get("freq", 1777),      minimum=1,      maximum=1e5)
    demod       = ListParameter( 'Modulation channel',      AC_chan,    default=section.get("demod", AC_chan[0]))
    y_min       = FloatParameter('From y',                  units='mm', default=section.get("y_min", 0))
    y_max       = FloatParameter('To y',                    units='mm', default=section.get("y_max", 0.1))
    y_step      = FloatParameter('Step',                    units='mm', default=section.get("y_step", 0.01))
    x           = FloatParameter('Position x',              units='mm', default=section.get("x", 0)) 
    b           = FloatParameter('Field ',                  units='A',  default=section.get("b", 0.),           minimum=-6,     maximum=6)

    # Only active if modulation is used
    rate        = FloatParameter('Sampling rate',           units='Hz', default=section.get("rate", 50000),     minimum=10,     maximum=1.25e6,
                                    group_by='demod', group_condition=lambda v: 'AC_Output1' in v or 'AC_Output2' in v)
    lockin_bw   = FloatParameter('Lockin bandwith',         units='Hz', default=section.get("lockin_bw", 50),   minimum=1,      maximum=1e5   ,
                                    group_by='demod', group_condition=lambda v: 'AC_Output1' in v or 'AC_Output2' in v)
    mod_amp     = FloatParameter('Modulation amplitude',    units='V',  default=section.get("mod_amp", 1),      minimum=0,      maximum=2     ,
                                    group_by='demod', group_condition=lambda v: 'AC_Output1' in v or 'AC_Output2' in v)
    
    # Active the correct input or the 2 of them if no modulation is required by the user
    cst_out1    = FloatParameter(f'Constant output 1 ({AC_ports.get("AC_Output1", "None")})',
                                 units='V', default=section.get("cst_out", 0),  minimum=0,  maximum=2,
                                 group_by='demod', group_condition=lambda v: v == 'None' or 'AC_Output2' in v)
    cst_out2    = FloatParameter(f'Constant output 2 ({AC_ports.get("AC_Output2", "None")})',
                                 units='V', default=section.get("cst_out", 0),  minimum=0,  maximum=2,
                                 group_by='demod', group_condition=lambda v: v == 'None' or 'AC_Output1' in v)

    # Define all the data columns which will be recorded in the procedure
    DATA_COLUMNS = [
        'Iteration',
        'X Position (mm)',
        'Y Position (mm)',
        'Magnetic Field (A)',
        'Magnetic Field (mT)',
        'Voltage R 1f (V)',
        'Voltage X 1f (V)',
        'Voltage Y 1f (V)',
        'Voltage theta 1f (V)',
        'Voltage R 2f (V)',
        'Voltage X 2f (V)',
        'Voltage Y 2f (V)',
        'Voltage theta 2f (V)',
        'Voltage DC (V)',
        'Voltage DC STD (V)',
        'Intensity (V)',
        'Intensity STD (V)']
    
    def set_sample_name(self, sample_name):
        self.sample_name = sample_name

    def startup(self):
        """
        Define the tasks to do at the procedure's startup
        """
        dac.reserved = True                             # Reserve the DAC to prevent other processes from using it
        hall_sensor.reserved = True                     # Reserve the Hall sensor to prevent other processes from using it
        hall_sensor.set_aquisition_time(self.acq_time)  # Reset the aquisition time of the hall sensor so it takes the same amount of time than the lockin
        proc_config.save_parameters_dict(self.name,     # Save the value of the parameters in the config file, in the B_sweep section, this is allow the programm to "remember" the last value used
                                         self._parameters)
        
        # Define the values of positions for the sweep
        self.y_values = np.linspace(self.y_min, self.y_max, int(np.abs(self.y_max-self.y_min)//self.y_step + 1), endpoint=True)
        
        # Set the values of the metadaa
        self.exp_type_md = "Sweep along x"
        self.sample_md   = self.sample_name
        self.nb_it_md    = len(self.y_values)
        
        # If the first value of the magnetic field is not zero, set it up and wait a 1s to be sure it is stable
        if (self.b != 0):
            log.info(f"Setting up magnetic field to {self.b}A, wait 1s")
            dac.set_outputs_and_reset([0., 0., self.b])   #This reset the tasks, hence it should be done before the setup
            time.sleep(1)
        
        # Go to the required position and wait for the motors to be stable
        log.info(f"Move stage to ({self.x}mm, {self.y_min}mm)")
        stage.move_x_to(self.x)
        stage.move_y_to(self.y_min)
        stage.wait_stable()
        
        # Setup the aquisition in the DAC with out parameters
        dac.setup_aquisition(modulation_channel=self.demod, frequency=self.freq, acquisition_time=self.acq_time, sampling_rate=self.rate, modulation_amp=self.mod_amp)
        dac.coils_output = self.b
        dac.dc_output    = [self.cst_out1, self.cst_out2]
        

    def execute(self):
        """
        Define the core of the procdure
        """
        log.info("Aquisition...")
        
        # Start sweeping loop
        for i in range(len(self.y_values)):
            
            # We move the stage to the next position
            stage.move_y_to(self.y_values[i])

            # We first trigger the task which will take aquisition time to be complete on the DAC
            dac.start_tasks()
            # In the mean time we read the magnetic field, it takes some time depending on the value of the filter. This is not super accurate because of the modulations be it is much quicker
            B_measurement = hall_sensor.read_mT()
            # Then we read the data on the DAC, read_data will wait for the task to be done first
            balanced_diodes_data, intensity_diode_data  = dac.read_data()
            balanced_diodes_DC                          = np.mean(balanced_diodes_data)

            # Demodulate the signal if there is a modulation
            if self.demod != 'None':
                balanced_diodes_1f = dac.demodulation(balanced_diodes_data, dac.reference_signal_1f, offset=balanced_diodes_DC, bandwith=self.lockin_bw, order=4.)
                balanced_diodes_2f = dac.demodulation(balanced_diodes_data, dac.reference_signal_2f, offset=balanced_diodes_DC, bandwith=self.lockin_bw, order=4.)
            else:
                balanced_diodes_1f = dict({ "X" : 0, "Y" : 0, "R" : 0, "theta" : 0 })
                balanced_diodes_2f = dict({ "X" : 0, "Y" : 0, "R" : 0, "theta" : 0 })

            data = {
                'Iteration':            i,
                'X Position (mm)':      stage.get_x_pos(),
                'Y Position (mm)':      stage.get_y_pos(),
                'Magnetic Field (A)':   self.b,
                'Magnetic Field (mT)':  B_measurement,
                'Voltage R 1f (V)':     balanced_diodes_1f["R"],
                'Voltage X 1f (V)':     balanced_diodes_1f["X"],
                'Voltage Y 1f (V)':     balanced_diodes_1f["Y"],
                'Voltage theta 1f (V)': balanced_diodes_1f["theta"],
                'Voltage R 2f (V)':     balanced_diodes_2f["R"],
                'Voltage X 2f (V)':     balanced_diodes_2f["X"],
                'Voltage Y 2f (V)':     balanced_diodes_2f["Y"],
                'Voltage theta 2f (V)': balanced_diodes_2f["theta"],
                'Voltage DC (V)':       balanced_diodes_DC,
                'Voltage DC STD (V)':   np.std(balanced_diodes_data),
                'Intensity (V)':        np.mean(intensity_diode_data),
                'Intensity STD (V)':    np.std(intensity_diode_data)
            }

            log.debug("Produced numbers: %s" % data)
            self.emit('results', data)
            self.emit('progress', 100 * i / len(self.y_values))

            if self.should_stop(): break

    def shutdown(self):
        """
        Define the tasks to do at the procedure's end
        """
        log.info("Aquisition done, turning off the outputs")     
        stage.move_x_to(self.x)
        stage.move_y_to(self.y_min)
        dac.set_outputs_and_reset([0., 0., 0.])   # Set outputs to 0 and reset the task
        hall_sensor.set_aquisition_time(0.5)      # Set a refresh time for live measurements
        dac.reserved = False                      # Free the DAC to allow other processes to use it
        hall_sensor.reserved = False              # Free the Hall sensor to allow other processes to use it