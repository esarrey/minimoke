"""
File:         classes/dac_class.py
Author:       Eliott Sarrey
Date:         June 2023
Email:        eliott.sarrey@gmail.com

Description:
    This file defines a DAC class for signal generation and acquisition.
    This class interface the National Instrument data aquisition card using the nidaqmx library.
"""

import nidaqmx
import numpy as np
from scipy.signal import butter, filtfilt

from src.classes import dac_config

class DAC:
    def __init__(self) -> None:
        """
        Initialize the DAC (Data Acquisition Device) object.
        """
        self.reserved       = False
        self.status_setup   = False
        self.enabled        = False
        self.dc_output      = [0., 0.]
        self.coils_output   = 0.
        self.setup_dac()

    def __del__(self) -> None:
        """
        Class destructor: close the task before deleting the object.
        """
        if not self.enabled: return

        self.output.close()
        self.input.close()

    def setup_acquisition(self, modulation_channel='None', frequency=431, sampling_rate=50000, acquisition_time=1., modulation_amp=1.) -> None:
        """
        Set up the acquisition parameters.

        Args:
            frequency (float):              The frequency of the reference signal.
            sampling_rate (float):          The sampling rate for acquisition.
            acquisition_time (float):       The duration of the acquisition.
            modulation_amp (list of float): The amplitude of the modulation signal.
        """
        self.f = frequency
        self.sampling_rate = sampling_rate
        self.modulation_amp = modulation_amp

        if modulation_channel != 'None':
            self.mod_chan = modulation_channel.split("(")[0].strip()
            self.set_acquisition_time(acquisition_time)
        else:
            self.mod_chan = None
            self.acquisition_time = acquisition_time

        if not self.enabled: return

        self.reset_tasks()

        print(f"Currently using {self.mod_chan} as the modulation channel")
        
        self.create_signals()
        self.output.timing.cfg_samp_clk_timing(sampling_rate, samps_per_chan=len(self.reference_signal_1f[0])+1)
        self.input.timing.cfg_samp_clk_timing( sampling_rate, samps_per_chan=len(self.reference_signal_1f[0])+1)
        self.status_setup = True
 
    def set_acquisition_time(self, acquisition_time) -> None:
        """
        Set the acquisition time to make sure it corresponds to an integer number of periods.
        Otherwise, taking the mean value over the aquisition time would be meaningless

        Args:
            acquisition_time (float): The duration of the acquisition.
        """
        N_periods = round(acquisition_time * self.f)
        self.acquisition_time = N_periods / self.f

    def create_signals(self) -> None:
        """
        Create the reference signals for the modulation.
        """
        # Define the time array
        t = np.linspace(0, self.acquisition_time, round(self.sampling_rate * self.acquisition_time), endpoint=False)

        # Reference signals at frequency f
        self.reference_signal_1f = [
            np.sin(2 * np.pi * self.f * t),                    # Reference signal at 1f
            np.sin(2 * np.pi * self.f * t + np.pi / 2)         # Reference signal + 90degrees at 1f
        ]

        # Reference signals at frequency 2f
        self.reference_signal_2f = [
            np.sin(2 * np.pi * 2 * self.f * t),                # Reference signal at 2f
            np.sin(2 * np.pi * 2 * self.f * t + np.pi / 2)     # Reference signal + 90degrees at 2f
        ]

    def setup_dac(self) -> None:
        """
        Set up the DAC (Data Acquisition Device) for output and input.
        Define a trigger on PFI0 on a rising edge
        """
        try:
            self.output = nidaqmx.Task('OutputTask')
            self.input = nidaqmx.Task('InputTask')
            self.enabled = True
        except:
            print("DAC not found!")
            return

        # Get the ports from the config file
        ports = dac_config.get_section("IO ports")

        self.output.ao_channels.add_ao_voltage_chan(ports.get("AC_Output1", "Dev1/"))
        self.output.ao_channels.add_ao_voltage_chan(ports.get("AC_Output2", "Dev1/"))
        self.output.ao_channels.add_ao_voltage_chan(ports.get("Coils_Output", "Dev1/"))
        self.input.ai_channels.add_ai_voltage_chan(ports.get("Balanced_diodes_Input", "Dev1/"))
        self.input.ai_channels.add_ai_voltage_chan(ports.get("Intensity_diode_Input", "Dev1/"))

        # Set the trigger to a rising edge on PFI0
        self.output.triggers.start_trigger.cfg_dig_edge_start_trig("/Dev1/PFI0", trigger_edge=nidaqmx.constants.Edge.RISING)
        self.input.triggers.start_trigger.cfg_dig_edge_start_trig("/Dev1/PFI0", trigger_edge=nidaqmx.constants.Edge.RISING)

    def start_tasks(self) -> None:
        """
        Start the input and output tasks.
        The modulation output depends on the value of mod_chan.
        The RMS is set at the end of the reference signal in order to keep a non-zero current of field
        between 2 measurements.
        """
        if not self.enabled: return

        self.input.start()

        if self.mod_chan == "AC_Output1":
            output_values = np.array([  np.concatenate([self.reference_signal_1f[0] * self.modulation_amp, [self.modulation_amp/np.sqrt(2)]]),
                                        np.full(len(self.reference_signal_1f[0])+1, self.dc_output[1]),
                                        np.full(len(self.reference_signal_1f[0])+1, self.coils_output)])
        elif self.mod_chan == "AC_Output2":
            output_values = np.array([  np.full(len(self.reference_signal_1f[0]+1), self.dc_output[0]),
                                        np.concatenate([self.reference_signal_1f[0] * self.modulation_amp, [self.modulation_amp/np.sqrt(2)]]),
                                        np.full(len(self.reference_signal_1f[0]+1), self.coils_output)])
        else:
            output_values = np.array([  np.full(len(self.reference_signal_1f[0]+1), self.dc_output[0]),
                                        np.full(len(self.reference_signal_1f[0]+1), self.dc_output[1]),
                                        np.full(len(self.reference_signal_1f[0]+1), self.coils_output)])

        self.output.write(output_values, auto_start=True)

        # Create the trigger signal
        with nidaqmx.Task() as task_trigger:
            task_trigger.do_channels.add_do_chan("/Dev1/PFI0")
            task_trigger.write([True, False], auto_start=True)

    def read_data(self) -> tuple:
        """
        Read the acquired data.

        Returns:
            tuple: A tuple containing the acquired data for channel ai0 and ai1, respectively.
        """
        if not self.enabled:
            np_points = round(self.sampling_rate * self.acquisition_time) + 1
            return np.full(np_points, 0.), np.full(np_points, 0.)

        # First make sure that the measurement is completly done
        self.output.wait_until_done()
        self.input.wait_until_done()

        # Read the measurement from the DAC buffer
        data_ai0, data_ai1 = self.input.read(number_of_samples_per_channel=len(self.reference_signal_1f[0])+1)

        # The last output element corresponds to the RMS of the reference signal
        # We should not use it for the average nor for the demodulation
        data_ai0.pop()
        data_ai1.pop()

        # Stop the tasks
        self.output.stop()
        self.input.stop()

        return data_ai0, data_ai1

    def reset_tasks(self) -> None:
        """
        Stop the input and output tasks and set status_setup to False to force a reset of
        the DAC parameters.
        """
        if not self.enabled: return

        # First make sure that the measurement is completly done
        self.output.wait_until_done()
        self.input.wait_until_done()

        # Indicate the the DAC needs to be setup again
        self.status_setup = False
        # Reset the outputs to 0.
        self.coils_output = 0.
        self.dc_output    = [0., 0.]

        # Stop the tasks
        self.output.stop()
        self.input.stop()

    def demodulation(self, data, ref_signal, bandwith, offset=0, order=3) -> dict:
        """
        Demodulate the acquired data using the reference signal.

        Args:
            data (numpy.ndarray):           The acquired data to demodulate
            ref_signal (numpy.ndarray):     The reference signal [sinus, cosinus]
            bandwith (float):               The bandwidth for signal filtering.
            order (int):                    The order of the Butter filter.

        Returns:
            Lockin: An instance of the Lockin enumeration with demodulation results.
        """

        # Generate the numerator and denominator polynomials of the low pass filter
        lp_filter_numerator, lp_filter_denominator = butter(order, bandwith, fs=self.sampling_rate, btype='low')

        # Multiply the data by the reference signal to extract the in-phase and the out-of-phase components
        X = np.multiply(data - offset, ref_signal[0])   # Multiply by a sinus for the in-phase component
        Y = np.multiply(data - offset, ref_signal[1])   # Multiply by a cosinus for the out-of-phase component

        # Apply a low-pass filter on the results
        X = filtfilt(lp_filter_numerator, lp_filter_denominator, np.concatenate([X, X, X]))[len(data):2 * len(data)]
        Y = filtfilt(lp_filter_numerator, lp_filter_denominator, np.concatenate([Y, Y, Y]))[len(data):2 * len(data)]

        # Return the RMS of X, Y and the signal (R) and its phase (theta)
        return dict({
            "X" : np.sqrt(np.mean(X ** 2)),
            "Y" : np.sqrt(np.mean(Y ** 2)),
            "R" : np.mean(np.sqrt(X ** 2 + Y ** 2)),
            "theta" : np.mean(np.arctan2(Y, X))
        })

    def set_outputs_and_reset(self, outputs) -> None:
        """
        Set the outputs for the DAC.

        Args:
            outputs (list of numpy.ndarray): The output values, should be a list of arrays for each channel
        """
        if not self.enabled: return

        self.reset_tasks()
        # Define an arbitray sampling rate, here 10kHz. For some reasons, we have to set the value at least twice so samps_per_chan=2
        self.output.timing.cfg_samp_clk_timing(10000, samps_per_chan=2)

        self.output.write(np.tile([outputs[0], outputs[1], outputs[2]], (2, 1)).T.tolist(), auto_start=True)

        # We then trigger the output
        with nidaqmx.Task() as task_trigger:
            task_trigger.do_channels.add_do_chan("/Dev1/PFI0")
            task_trigger.write([True, False], auto_start=True)