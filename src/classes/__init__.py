"""
File:         classes/__init__.py
Author:       Eliott Sarrey
Date:         June 2023
Email:        eliott.sarrey@gmail.com

Description:
    This file imports the necessary modules for all the classes.
    Initialize some objects as a global variables used in the application.

"""

from .config_handler import proc_config, dac_config, stage_config
from .dac_class import DAC
from .hallsensor_class import HallSensor
from .statusbar_class import StatusBarHandler, logging
from .stage_class import Stage


# Initialize the DAC, HallSensor, Stage, and logging objects
dac = DAC()
hall_sensor = HallSensor()
stage = Stage()
log = logging.getLogger(__name__)

log.setLevel(logging.INFO)