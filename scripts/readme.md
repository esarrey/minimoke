# Scripts
## Debug

The debug batch script is used for debug only, this using directly using the files in the 'src' folder.
This is useful to quickly check what you modified.

## Build

The build batch script is used to generate an executable file from the source files.
This allow to properly generate the sofware and to compile (more stable and faster).

After running the script, a window should be opened in your browser. 
In 'Settings', import the json Confif file from 'scripts', choose the build folder for the output and then click on 'Convert py to exe'
