# File:         __main__.py
# Author:       Eliott Sarrey
# Date:         June 2023
# Email:        eliott.sarrey@gmail.com
# 
# ███╗   ███╗██╗███╗   ██╗██╗    ███╗   ███╗ ██████╗ ██╗  ██╗███████╗
# ████╗ ████║██║████╗  ██║██║    ████╗ ████║██╔═══██╗██║ ██╔╝██╔════╝
# ██╔████╔██║██║██╔██╗ ██║██║    ██╔████╔██║██║   ██║█████╔╝ █████╗  
# ██║╚██╔╝██║██║██║╚██╗██║██║    ██║╚██╔╝██║██║   ██║██╔═██╗ ██╔══╝  
# ██║ ╚═╝ ██║██║██║ ╚████║██║    ██║ ╚═╝ ██║╚██████╔╝██║  ██╗███████╗
# ╚═╝     ╚═╝╚═╝╚═╝  ╚═══╝╚═╝    ╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝
#
#
# Description:
#   This is the software for the Mini-MOKE setup of the Spintronics lab at ETHZ
#   The software is based on the pymeasure libraries
#   For the purpose of the software, some classes had to be rewritten and improved, like the UIWindow class
#   In order to keep it clean, the library itself has been left unchanged and the new classes are included in the src folder
# 
#   If you have any question regarding this software, please reach out to the author.


import sys
import qdarkstyle
from pymeasure.experiment import Results, unique_filename
from pymeasure.display.Qt import QtWidgets
from PyQt5.QtGui import QIcon

from src.ui         import UIWindow, UserManualTab, LiveTab, MotorsTab
from src.procedures import B_Sweep, X_Sweep, Y_Sweep, XY_Sweep

class MainWindow(UIWindow):
    """
    Class of for the main window of the sofware, defining the input section, the graph sections and all the tabs
    """
    def __init__(self):
        super().__init__(
            procedure_class=[B_Sweep, X_Sweep, Y_Sweep, XY_Sweep],
            inputs=[
                ['acq_time','freq','demod','rate','lockin_bw','mod_amp','cst_out1','cst_out2','b_min','b_max','b_step','x','y','full_hyste'],
                ['acq_time','freq','demod','rate','lockin_bw','mod_amp','cst_out1','cst_out2','x_min','x_max','x_step','y','b'],
                ['acq_time','freq','demod','rate','lockin_bw','mod_amp','cst_out1','cst_out2','y_min','y_max','y_step','x','b'],
                ['acq_time','freq','demod','rate','lockin_bw','mod_amp','cst_out1','cst_out2','x_min','x_max','x_step','y_min','y_max','y_step','b']
                ],
            displays=['acq_time', 'freq'],
            x_axis='Iteration',
            y_axis='Voltage DC (V)',
            widget_list=(UserManualTab("User Manual"), MotorsTab("Motor control"), LiveTab("Live measurements")),
            directory_input=True
        )
        self.setWindowTitle('Mini MOKE')
        self.setWindowIcon(QIcon('assets/icon.ico'))

    def queue(self, procedure=None):
        """
        Queue the next experiment and save it to the data folder
        """

        if procedure is None: procedure = self.make_procedure()

        file = unique_filename(directory=self.directory,
                               prefix=self.sample_name_line.text()+"_"+procedure.name+"_",
                               dated_folder=True,
                               datetimeformat="%Y-%m-%d-%Hh%M"
                               )
        results = Results(procedure, file)
        experiment = self.new_experiment(results)

        self.manager.queue(experiment)      # Queue the experiment


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    dark_stylesheet = qdarkstyle.load_stylesheet_pyqt5()
    app.setStyleSheet(dark_stylesheet)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())